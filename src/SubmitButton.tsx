function SubmitButton() {
    return (
        <div className="relative">
            <button 
                className="rounded-full py-2 px-4 bg-gradient-to-r from-pink-500 to-pink-900 text-white shadow-md hover:shadow-lg focus:outline-none focus:ring-2 focus:ring-purple-500 focus:ring-opacity-50 transition duration-300 ease-in-out bottom-0 right-0 transform hover:scale-105 whitespace-normal"
            >
                Submit
            </button>
        </div>
    );
  }
  
  export default SubmitButton;
  
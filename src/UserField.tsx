import React, { useState, useRef } from 'react';

interface UserFieldProps {
    title: string;
    placeholder: string;
}

const UserField: React.FC<UserFieldProps> = ({ title, placeholder }) => {
    const [value, setValue] = useState('');
    const textareaRef = useRef<HTMLTextAreaElement>(null);
    
    const handleChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
        setValue(event.target.value);
        // Dynamically adjust the height of the textarea as text is entered
        event.target.style.height = 'auto';
        event.target.style.height = event.target.scrollHeight + 'px';
    };

    return (
        <div className="rounded-xl border border-gray-700 p-0 mt-5 mb-5 m-8 text-primary w-full md:w-full">
            {/* Form title */}
            {title && <div className="text-base font-semibold p-1 px-2">{title}</div>}
            
            {/* Horizontal line */}
            <hr className="border-gray-700 mb-0" />
            
            {/* Textarea field with placeholder */}
            <div className="">
                <textarea 
                    ref={textareaRef}
                    className="w-full p-0.5 rounded-xl focus:outline-none bg-transparent overflow-y-auto whitespace-normal resize-none"
                    placeholder={placeholder}
                    value={value}
                    onChange={handleChange}
                    rows={1} // Initial row count
                />
            </div>
        </div>
    );
}

export default UserField;

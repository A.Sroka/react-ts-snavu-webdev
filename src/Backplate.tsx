import SubmitButton from './SubmitButton';
import UserField from './UserField';
import './ScrollbarStyles.css';
import Banner from './Banner';

function Backplate() {
    return (
        <div className="flex justify-center items-center h-full">
            <div className="flex flex-col justify-center items-center rounded-2xl bg-neutral-950  w-5/12 h-5/6 overflow-y-auto px-5 overflow-x-hidden max-h-5/6">
                {/*<Banner />*/}
                {/* UserField components */}
                <UserField title="Enter your text:" placeholder="Please enter your text...1" />
                <UserField title="Enter your text:" placeholder="Please enter your text...2" />
                <UserField title="Enter your text:" placeholder="Please enter your text...3" />
                <UserField title="Enter your text:" placeholder="Please enter your text...4" />
                <UserField title="Enter your text:" placeholder="Please enter your text...5" />
                <UserField title="Enter your text:" placeholder="Please enter your text...6" />
                <UserField title="Enter your text:" placeholder="Please enter your text...7" />
                <UserField title="Enter your text:" placeholder="Please enter your text...8" />
                <UserField title="Enter your text:" placeholder="Please enter your text...9" />
                <UserField title="Enter your text:" placeholder="Please enter your text...10" />
                
                
                
                <SubmitButton />
            </div>
        </div>
    );
}

export default Backplate;
